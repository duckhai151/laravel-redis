<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function index(Request $request) {
        $key = md5(vsprintf('%s.%s.%s', [
            'PostController',
            'index',
            $request->get('page', 1),
        ]));
        
        $posts = Cache::remember($key, 100, function () {
            return Post::paginate();
        });

        return view('test', ['posts' => $posts]);
    }    
}
